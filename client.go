package safeclient

import (
	"context"
	"errors"
	"net"
	"net/http"
	"time"
	"fmt"

	"github.com/projectdiscovery/networkpolicy"
)

func DefaultNetworkPolicy() (*networkpolicy.NetworkPolicy, error) {
	var npOptions networkpolicy.Options
	// https://github.com/projectdiscovery/networkpolicy/blob/main/cidr.go
	npOptions.DenyList = append(npOptions.DenyList, networkpolicy.DefaultIPv4DenylistRanges...)
	npOptions.DenyList = append(npOptions.DenyList, networkpolicy.DefaultIPv6DenylistRanges...)
	// Allow only HTTP and HTTPS schemes
	npOptions.AllowSchemeList = networkpolicy.DefaultSchemeAllowList
	np, err := networkpolicy.New(npOptions)
	if err != nil {
		return nil, err
	}

	return np, nil
}

func New(np *networkpolicy.NetworkPolicy, maxRedirects uint) *http.Client {
	dialer := &net.Dialer{ Timeout: 10 * time.Second }
	transport := http.DefaultTransport.(*http.Transport).Clone()
	transport.DialContext = func(ctx context.Context, network, addr string) (net.Conn, error) {
		host, port, err := net.SplitHostPort(addr)
		if err != nil {
			return nil, err
		}

		ip, ok := np.ValidateHost(host)
		if !ok {
			return nil, errors.New("could not find a valid IP to dial to")
		}

		addr = net.JoinHostPort(ip, port)
		con, err := dialer.DialContext(ctx, network, addr)
		if err != nil {
			return nil, err
		}

		return con, nil
	}

	return &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			if len(via) >= int(maxRedirects) {
				return fmt.Errorf("too many redirects (%d)", len(via))
			}

			if req.URL.Hostname() != via[len(via)-1].URL.Hostname() {
				return fmt.Errorf("redirect to different domain (%s)", req.URL.Hostname())
			}

			if req.Response.StatusCode == 307 || req.Response.StatusCode == 308 {
				return fmt.Errorf("unsupported redirect (code %d)", req.Response.StatusCode)
			}

			_, ok := np.ValidateHost(req.URL.Hostname())
			if !ok {
				return fmt.Errorf("redirect to forbidden target (%s)", req.URL.Hostname())
			}

			return nil
		},
		Timeout: 30 * time.Second,
		Transport: transport,
	}
}
