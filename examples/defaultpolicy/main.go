package main

import (
	"log"
	"os"
	"net/http"
	"crypto/tls"

	"gitlab.com/hounder/safeclient"
)

func main() {
	networkPolicy, err := safeclient.DefaultNetworkPolicy()
	if err != nil {
		log.Fatalf("Could not create network policy: %v", err)
	}

	safeClient := safeclient.New(networkPolicy, 5)
	safeClient.Transport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	resp, err := safeClient.Get(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("%d\n", resp.StatusCode)
}
